import time
import RPi.GPIO as GPIO
import Adafruit_ADS1x15

umidade_atual = 0
umidade_desejada = 0
tempo_aberto_anterior = 0
tempo_aberto_atual = 0
diferenca_umidade = umidade_desejada - umidade_atual
fim=0
ini = time.time()
i=0
tempo_coleta = 300
pam = pam(area_molhada_emissor=0, area_total_planta=0)
capacidade_campo = 0
profundidade_radicular = 0
densidade_solo = 0
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
adc = Adafruit_ADS1x15.ADS1115()
adc.start_adc(0, gain=1)
umidade_atual = coletarUmidade(adc.get_last_result())
arquivo = open("teste.txt","a")


while(1):
    if((fim - ini)>tempo_coleta):
        ini = time.time()
        #Coletar dados
        umidade_atual = coletarUmidade(adc.get_last_result())
        irn = irn() #colocar os dados
        if(irn>5):
            irn = irn*0.005
        elif(irn>3 and irn <= 5):
            irn = irn*0.02
        elif(irn >1 and irn <= 3):
            irn = irn*0.03
        elif(irn> 0.5 and irn <= 1):
            irn = irn*0.07
        elif(irn > 0.1 and irn <= 0.5):
            irn = irn*0.15
        elif(irn > 0.05 and irn < 0.1):
            irn = irn*0.2
        elif(irn < 0.05):
            irn = irn*0.01
        janela_irrigacao(irn/vazao, 60, 300)
        arquivo.writelines("quantidade de água"+";"+"luminosidade"+";"+"temperatura"+";"+str(vazao)+";"+str(umidade_atual)+";"+"\n")
        arquivo.close()
    fim = time.time()



def diferenca_positiva(diferenca_umidade, tempo_aberto_anterior):
	if((diferenca_umidade) > 1 and (diferenca_umidade) < 5):
		tempo_aberto_atual = tempo_aberto_anterior * 1.1
	elif(diferenca_umidade > 5 and diferenca_umidade < 10):
		tempo_aberto_atual = tempo_aberto_anterior * 1.5
	elif(diferenca_umidade< 10):
		tempo_aberto_atual = tempo_aberto_anterior * 2
	return tempo_aberto_atual;

def diferenca_negativa(tempo_aberto_anterior, tempo_aberto_anterior2):
	return (tempo_aberto_anterior2 + tempo_aberto_anterior)/2

def irrigar_por(tempo):
    inicio = time.time()
    fim = 0
    #Ativar irrigação
    GPIO.output(18,GPIO.HIGH)
    while((fim - inicio)<tempo):
        fim = time.time()
    #desativar irrigação
    GPIO.output(18,GPIO.LOW)
    print("irrigou por " + str(tempo)+" segundos")

def janela_irrigacao(tempo_aberto, tempo_janela, tempo_total):
    qtd_janelas = int(tempo_total/tempo_janela)
    i=0
    inicio = time.time()
    fim = 0
    while(i<qtd_janelas):
        if(fim - inicio > tempo_janela):
            inicio = time.time()
            irrigar_por(tempo_aberto)
        fim = time.time()

def pam(area_molhada_emissor, area_total_planta):
    #area_molhada_emissor - metros quadrados
    #area_total_planta - metros quadrados
    return (area_molhada_emissor/area_total_planta)*100

def irn(pam, capacidade_campo, umidade_atual, profundidade_radicular, densidade_solo):
    #capacidade de campo - % em peso
    #umidade atual - % em peso
    #profundidade radicular - centímetros - para a alface de 20 a 30 cm
    #densidade_solo - gramas/centímetro cúbico
    return (((capacidade_campo-umidade_atual)/10)*densidade_solo*profundidade_radicular*(pam/100))/0.8

def calcularUmidade(lista):
    soma = 0
    for valor in lista:
        soma += valor
    return (somar/len(lista))
        







