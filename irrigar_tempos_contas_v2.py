import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

'''
Relação de pinos

Sensor de umidade no pino 0 : Valvula que está no pino 10



'''

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)

umidades = []
tempos = [0,0,0,0,0,0,0,0,0]
diferencas_umidade = []
umidades_desejadas = [0,0,0,0,0,0,0,0,0]
tempo_aberto_anterior = [0,0,0,0,0,0,0,0,0]
tempo_aberto_atual = [0,0,0,0,0,0,0,0,0]
sensores = [AnalogIn(ads, ADS.P0),AnalogIn(ads, ADS.P1),AnalogIn(ads, ADS.P2),AnalogIn(ads, ADS.P3),AnalogIn(ads, ADS.P4),AnalogIn(ads, ADS.P5),AnalogIn(ads, ADS.P6),AnalogIn(ads, ADS.P7),AnalogIn(ads, ADS.P8)]
#ADS.P0 é mesma coisa que 0
for sensor in sensores:
    umidades.append(converterUmidade(sensor.voltage))
i=0
for i in range(9):
    diferencas_umidade.append(umidades_desejadas[i] - umidades[i])


fim=0
ini = time.time()
i=0
tempo_coleta = 300
pam = pam(area_molhada_emissor=0, area_total_planta=0)
capacidade_campo = 0 #Utilizar em termos de % (x100)
profundidade_radicular = 0
densidade_solo = 0
arquivo = open("teste.txt","a")
umidades = umidades.clear()


while(1):
    if((fim - ini)>tempo_coleta):
        ini = time.time()
        #Coletar dados
        j=0
        vazao = 1 #Coletar vazao
        sensores = [AnalogIn(ads, ADS.P0),AnalogIn(ads, ADS.P1),AnalogIn(ads, ADS.P2),AnalogIn(ads, ADS.P3),AnalogIn(ads, ADS.P4),AnalogIn(ads, ADS.P5),AnalogIn(ads, ADS.P6),AnalogIn(ads, ADS.P7),AnalogIn(ads, ADS.P8)]
        for j in range(9):
            umidades[j]=converterUmidade(sensor[j].voltage)
            irn = irn(pam, capacidade_campo, umidades[j],profundidade_radicular,densidade_solo) #colocar os dados
            if(irn>5):
                irn = irn*0.005
            elif(irn>3 and irn <= 5):
                irn = irn*0.02
            elif(irn >1 and irn <= 3):
                irn = irn*0.03
            elif(irn> 0.5 and irn <= 1):
                irn = irn*0.07
            elif(irn > 0.1 and irn <= 0.5):
                irn = irn*0.15
            elif(irn > 0.05 and irn < 0.1):
                irn = irn*0.2
            elif(irn < 0.05):
                irn = irn*0.01
            tempos[j] = irn/vazao
        janela_irrigacao(tempos, 60, 300)
        arquivo.writelines("quantidade de água"+";"+"luminosidade"+";"+"temperatura"+";"+str(vazao)+";"+str(umidade_atual)+";"+"\n")
        arquivo.close()
    fim = time.time()



def diferenca_positiva(diferenca_umidade, tempo_aberto_anterior):
	if((diferenca_umidade) > 1 and (diferenca_umidade) < 5):
		tempo_aberto_atual = tempo_aberto_anterior * 1.1
	elif(diferenca_umidade > 5 and diferenca_umidade < 10):
		tempo_aberto_atual = tempo_aberto_anterior * 1.5
	elif(diferenca_umidade< 10):
		tempo_aberto_atual = tempo_aberto_anterior * 2
	return tempo_aberto_atual;

def diferenca_negativa(tempo_aberto_anterior, tempo_aberto_anterior2):
	return (tempo_aberto_anterior2 + tempo_aberto_anterior)/2

def irrigar_por(tempo):
    inicio = time.time()
    fim = 0
    #Ativar irrigação
    GPIO.output(18,GPIO.HIGH)
    while((fim - inicio)<tempo):
        fim = time.time()
    #desativar irrigação
    GPIO.output(18,GPIO.LOW)
    print("irrigou por " + str(tempo)+" segundos")

def janela_irrigacao(tempos, tempo_janela, tempo_total):
    indices = [[0,tempos[0]],[1,tempos[1]],[2,tempos[2]],[3,tempos[3]],[4,tempos[4]],[5,tempos[5]],[6,tempos[6]],[7,tempos[7]],[8,tempos[8],[9,tempos[9]]]]
    #Abrir valvulas
    iterador = 0
    for iterador in range(9):
        if tempos[iterador] == 0 :
            continue
        GPIO.output(iterador, GPIO.HIGH)
    tempos = sorted(tempos)
    #Fechar valvulas
    k=0
    inicio = time.time()
    while(k<9):
        fim = time.time()
        if(fim - inicio) < tempos[k]:
            k+=1
            for indice in indices:
                if indice[1] == tempos[k]:
                    GPIO.output(indice[1], GPIO.LOW)


def pam(area_molhada_emissor, area_total_planta):
    return (area_molhada_emissor/area_total_planta)*100

def irn(pam, capacidade_campo, umidade_atual, profundidade_radicular, densidade_solo):
    return (((capacidade_campo-umidade_atual)/10)*densidade_solo*profundidade_radicular*(pam/100))/0.8

def calcularUmidade(lista):
    soma = 0
    for valor in lista:
        soma += valor
    return (somar/len(lista))

def converterUmidade(valor):
    return (valor/3)*100

