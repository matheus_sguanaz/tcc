import time

umidade_atual = 0
umidade_desejada = 0
tempo_aberto_anterior = 0
tempo_aberto_atual = 0
diferenca_umidade = umidade_desejada - umidade_atual
fim=0
ini = time.time()
i=0
tempo_coleta = 300

while(1):
    #if((fim - ini)>tempo_coleta):
    ini = time.time()
    #Coletar dados
    tempo_aberto_anterior = tempo_aberto_atual
    if(diferenca_umidade>0):
        tempo_aberto_atual = diferenca_positiva(diferenca_umidade, tempo_aberto_atual)
        janela_irrigacao(tempo_aberto_atual,20,300)
    else:
        tempo_aberto_atual = diferenca_negativa(tempo_aberto_atual, tempo_aberto_anterior)
        janela_irrigacao(tempo_aberto_atual,20,300)
    #fim = time.time()



def diferenca_positiva(diferenca_umidade, tempo_aberto_anterior):
	if((diferenca_umidade) > 1 and (diferenca_umidade) < 5):
		tempo_aberto_atual = tempo_aberto_anterior * 1.1
	elif(diferenca_umidade > 5 and diferenca_umidade < 10):
		tempo_aberto_atual = tempo_aberto_anterior * 1.5
	elif(diferenca_umidade< 10):
		tempo_aberto_atual = tempo_aberto_anterior * 2
	return tempo_aberto_atual;

def diferenca_negativa(tempo_aberto_anterior, tempo_aberto_anterior2):
	return (tempo_aberto_anterior2 + tempo_aberto_anterior)/2

def irrigar_por(tempo):
    inicio = time.time()
    fim = 0
    #Ativar irrigação
    while((fim - inicio)<tempo):
        fim = time.time()
    #desativar irrigação
    print("irrigou por " + str(tempo)+" segundos")

def janela_irrigacao(tempo_aberto, tempo_janela, tempo_total):
    qtd_janelas = int(tempo_total/tempo_janela)
    i=0
    inicio = time.time()
    fim = 0
    while(i<qtd_janelas):
        if(fim - inicio > tempo_janela):
            inicio = time.time()
            irrigar_por(tempo_aberto)
        fim = time.time()
        







