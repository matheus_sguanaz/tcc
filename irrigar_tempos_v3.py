import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)
umidades = []
sensores = []
sensores[0] = AnalogIn(ads, ADS.P0)
sensores[1] = AnalogIn(ads, ADS.P1)
sensores[2] = AnalogIn(ads, ADS.P2)
sensores[3] = AnalogIn(ads, ADS.P3)
sensores[4] = AnalogIn(ads, ADS.P4)
sensores[5] = AnalogIn(ads, ADS.P5)
sensores[6] = AnalogIn(ads, ADS.P6)
sensores[7] = AnalogIn(ads, ADS.P7)
sensores[8] = AnalogIn(ads, ADS.P8)

for sensor in sensores:
    umidades.append(converterUmidade(sensor.voltage))

umidade_atual = calcularUmidade(umidades)
umidade_desejada = 0
tempo_aberto_anterior = 0
tempo_aberto_atual = 0
diferenca_umidade = umidade_desejada - umidade_atual
fim=0
ini = time.time()
i=0
tempo_coleta = 300

while(1):
    ini = time.time()
    #Coletar dados
    tempo_aberto_anterior = tempo_aberto_atual
    if(diferenca_umidade>0):
        tempo_aberto_atual = diferenca_positiva(diferenca_umidade, tempo_aberto_atual)
        janela_irrigacao(tempo_aberto_atual,20,300)
    else:
        tempo_aberto_atual = diferenca_negativa(tempo_aberto_atual, tempo_aberto_anterior)
        janela_irrigacao(tempo_aberto_atual,20,300)



def diferenca_positiva(diferenca_umidade, tempo_aberto_anterior):
	if((diferenca_umidade) > 1 and (diferenca_umidade) < 5):
		tempo_aberto_atual = tempo_aberto_anterior * 1.1
	elif(diferenca_umidade > 5 and diferenca_umidade < 10):
		tempo_aberto_atual = tempo_aberto_anterior * 1.5
	elif(diferenca_umidade< 10):
		tempo_aberto_atual = tempo_aberto_anterior * 2
	return tempo_aberto_atual;

def diferenca_negativa(tempo_aberto_anterior, tempo_aberto_anterior2):
	return (tempo_aberto_anterior2 + tempo_aberto_anterior)/2

def irrigar_por(tempo):
    inicio = time.time()
    fim = 0
    #Ativar irrigação
    GPIO.output(18,GPIO.HIGH)

    while((fim - inicio)<tempo):
        fim = time.time()
    #desativar irrigação
    GPIO.output(18,GPIO.LOW)
    print("irrigou por " + str(tempo)+" segundos")

def janela_irrigacao(tempo_aberto, tempo_janela, tempo_total):
    qtd_janelas = int(tempo_total/tempo_janela)
    i=0
    inicio = time.time()
    fim = 0
    while(i<qtd_janelas):
        if(fim - inicio > tempo_janela):
            inicio = time.time()
            irrigar_por(tempo_aberto)
        fim = time.time()

def pam(area_molhada_emissor, area_total_planta):
    return (area_molhada_emissor/area_total_planta)*100

def irn(pam, capacidade_campo, umidade_atual, profundidade_radicular, densidade_solo):
    return (((capacidade_campo-umidade_atual)/10)*densidade_solo*profundidade_radicular*(pam/100))/0.8

def calcularUmidade(lista):
    soma = 0
    for valor in lista:
        soma += valor
    return (somar/len(lista))

def converterUmidade(valor):
    return (valor/3)*100
        







