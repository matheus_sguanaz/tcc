class Constantes:
    capacidade_campo = 70 #% em peso
    profundidade_radicular = 23 #profundidade radicular - centímetros - para a alface de 20 a 30 cm
    densidade_solo = 1 #densidade_solo - gramas/centímetro cúbico
    area_molhada_emissor = 1  #area_molhada_emissor - metros quadrados
    area_total_planta = 1 #area_total_planta - metros quadrados
    vazao_gotejador = 2 #litros por segundo
    volume_cano = 1 #litros
    nivel_agua = 2 #centímetros