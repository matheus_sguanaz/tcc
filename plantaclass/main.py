from planta import Planta
import alertaemail as smail 
from constantes import Constantes as cnt
import sys
from datetime import datetime
#from sklearn.model_selection import train_test_split
#import pandas as pd
import psycopg2
import time
import sensEatu as sea


#Conexão banco de dados
conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
cursor = conn.cursor()
cursor.execute("SELECT * FROM plantas order by id_planta")
result = cursor.fetchall()
plantas = []
temperatura = 0
umidade_ar = 0
dados_umidade = []
mLitros = 0
umidade_1 = 0
alert_temperatura = False
alert_umidade_ar = False
flag_tempo_rega_1 = True
flag_tempo_rega_2 = True
tempo_rega_1 = 0
tempo_rega_2 = 0
antigo_tempo_rega_1 = 0
antigo_tempo_rega_2 = 0
tempo_acumulado_1 = 0
tempo_acumulado_2 = 0
timetemp_1 = 0
conn.close()



#Inicialização para aprendizado de máquina
'''dados_regressao = pd.read_csv('dados_regressao.csv')#Criar arquivo com os valores corretos para utilização do modelo
X = dados_regressao[['Avg. umidade do solo', 'Avg. umidade do ar', 'Avg. temperatura','Avg. luminosidade', 'Avg. tempo plantio']]
Y = dados_regressao[['quantidade agua']]
X_train, X_teste, Y_train, Y_teste = train_test_split(X,Y, test_size = 0.5, random_state=101)
lm = LinearRegression()
lm.fit(X_train, Y_train)'''

def coletar_umidades(plantas):
    for i in range(len(plantas)):
        if(int(plantas[i].pino_sensor)>3):
            plantas[i].calcular_umidade(tensao=1.7)
        else:
            plantas[i].calcular_umidade(tensao=1.7)
    return plantas

def definir_lrn(lrn,periodo,vazao_gotejador):#Multiplicar por 15
    if(lrn>50):#lrn em  milimetros cúbicos
        lrn = ((vazao_gotejador)*(periodo-30)) #maximo possível dentro do período
    else:
        lrn = lrn * 0.0035
    '''elif(lrn>39 and lrn <= 50):#Entre 195 mL e 250 mL
        lrn = lrn*0.005
    elif(lrn>26 and lrn <= 39):#Entre 88 mL e 132 mL
        lrn = lrn*0.0034
    elif(lrn >20 and lrn <= 26):#Entre 80 mL e 104 mL
        lrn = lrn*0.004
    elif(lrn> 13 and lrn <= 20):#Entre 45 mL e 70 mL
        lrn = lrn*0.0035
    elif(lrn > 8 and lrn <= 13): #Entre 16 mL e 26 mL
        lrn = lrn*0.002
    elif(lrn > 3 and lrn < 8): #Entre  4.5 mL e 12 mL
        lrn = lrn*0.0015
    elif(lrn < 3):#Menor que 3 mL
        lrn = lrn*0.001'''
    return lrn


tempo_coleta_dados = 300

for linha in result:
    planta = Planta(id=linha[0],pino_sensor=linha[1],pino_atuador=linha[2],periodo_rega=linha[3],tempo_rega=linha[4],umidade_desejada=linha[5],umidade_atual=linha[6],tempo_plantio=linha[8],modo_operacao=linha[7],qtd_agua=linha[9], flag_novo_modo = linha[10])
    plantas.append(planta)

plantas = coletar_umidades(plantas)

ini = 0
ini_select = 0#time.time()
ini_definicao_parametros_iniciais = time.time() + 610
try:
    while(1):
        try:
            fim_definicao_parametros_iniciais = time.time()
            if(fim_definicao_parametros_iniciais - ini_definicao_parametros_iniciais > 200):
                string = "("
                for i in range(len(plantas)):
                    string+=str(plantas[i].id)+","
                string+=")"
                string = string.replace(",)",")")
                try:
                    conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc") #Talvez aqui
                    cursor = conn.cursor()
                    cursor.execute("SELECT * FROM plantas where id_planta not in "+string)
                    result = cursor.fetchall()
                except:
                    print("Falha na conexao com o BD\n")
                    result = []
                #Verificar se foi adicionado novos objetos plantas no banco, caso sim cria-se o objeto no programa
                for linha in result:
                    if result!=[]:
                        planta = Planta(id=linha[0],pino_sensor=linha[1],pino_atuador=linha[2],periodo_rega=linha[3],tempo_rega=linha[4],umidade_desejada=linha[5],umidade_atual=linha[6],tempo_plantio=linha[8],modo_operacao=linha[7], qtd_agua=linha[9], flag_novo_modo = linha[10])
                        plantas.append(planta)
                cursor.execute("SELECT tempo_plantio from plantas order by id_planta")
                result = cursor.fetchall()
                for i in range(len(plantas)):
                    plantas[i].tempo_plantio = int(result[i][0])
                conn.close()


            fim = time.time()
            fim_select = time.time()
            if((fim_select - ini_select) > 10):
                ini_select = time.time()
                conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                cursor = conn.cursor()
                cursor.execute("SELECT modo_operacao,periodo_rega, tempo_rega, qtd_agua, flag_novo_modo FROM plantas order by id_planta")
                result = cursor.fetchall()
                #Modo de operação
                #0 - manual
                #1 - automatico
                #2 - pausado
                #3 - aprendizado de máquina
                for i in range(len(plantas)):
                    plantas[i].modo_operacao = result[i][0]
                    if(plantas[i].modo_operacao == 0):
                        plantas[i].periodo_rega = result[i][1]
                        plantas[i].tempo_rega = result[i][2]
                        plantas[i].qtd_agua = result[i][3]
                        plantas[i].flag_novo_modo = result[i][4]
                    if(plantas[i].modo_operacao == 2):
                        plantas[i].tempo_rega = 0
                        plantas[i].periodo_rega = 0
                    if(plantas[i].modo_operacao == 1 or plantas[i].modo_operacao ==3):
                        plantas[i].periodo_rega = result[i][1]
                        #plantas[i].tempo_rega = result[i][2]
                        plantas[i].flag_novo_modo = result[i][4]
                conn.close()
                for i in range(len(plantas)):
                    if(plantas[i].flag_novo_modo == 1):
                        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                        cursor = conn.cursor()
                        cursor.execute("UPDATE plantas set flag_novo_modo = 0 where id_planta="+str(plantas[i].id))
                        plantas[i].periodo_rega_inicio = 0
                        plantas[i].tempo_rega = 0
                        conn.commit()
                        conn.close()
                
            if((fim - ini) > tempo_coleta_dados):
                ini = time.time()
                #Coletar dados
                

                #if(plantas[0].modo_operacao!=2):
                dados_umidade, temperatura,luminosidade_baixo, luminosidade_cima, umidade_ar, uv  = sea.coletar_dados(plantas)
                umidade_1 = (dados_umidade[0]+dados_umidade[1])/2
                for i in range(len(plantas)):
                    #plantas[i].umidade_atual = dados_umidade[i]
                    plantas[i].umidade_atual = umidade_1
                    if(plantas[i].id==3 or plantas[i].id==4):
                        plantas[i].umidade_atual = (dados_umidade[2] + dados_umidade[3])/2
                vazao = 1 #litros por segundo
                ini = time.time()
                if(temperatura > 30 and alert_temperatura == False):
                    alert_temperatura = True
                    try:
                        smail.enviar("Temperatura maior que 30 °C")
                    except:
                        print("Não foi possível enviar o email\n")
                else:
                    alert_temperatura = False
                if(umidade_ar > 80 and alert_umidade_ar == False):
                    alert_umidade_ar = True
                    try:
                        smail.enviar("Umidade do ar maior que 80")
                    except:
                        print("Não foi possível enviar o email\n")
                else:
                    alert_umidade_ar = False

                #Coleta do tempo que as solenoides ficaram abertas
                conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                cursor = conn.cursor()
                cursor.execute("select sum(quantidade_agua) from dados where dat_gravacao between (select dat_gravacao from tempos_vazao where tempo = 0 order by dat_gravacao desc limit 1) and current_timestamp")
                result = cursor.fetchall()
                conn.close()
                try:
                    miliLitros = float(result[0][0])*1000
                except:
                    miliLitros = 0
                #Definição de tempo de rega
                for i in range(len(plantas)):
                    #Vazão está sendo guardada em mL/s e quantidade de água em litros
                    #Fórmula com base no tempo, utilizar vazao/2 para vazao, Alterar aqui
                    try:
                        if(int(plantas[i].pino_atuador)==23):
                            mLitros  = miliLitros
                            vazao = (60.2862794402195-(0.000866903317118373*mLitros)+(0.0000000058674550482351*mLitros*mLitros)-(0.0000000000103544854360592*mLitros*mLitros*mLitros))/60
                        else:
                            mLitros  = miliLitros
                            vazao = (52.7209804450539-(0.000830712579866397*mLitros)-(0.00000000746003172622697*mLitros*mLitros)-(0.00000000000639293147077318*mLitros*mLitros*mLitros))/60
                    except:
                        print("Deu erro\n")
                        #Alterar aqui, na primeira vez, não terá dados
                        if(int(plantas[i].pino_atuador)==23):
                            vazao = 60.2862794402195/60
                        else:
                            vazao = 52.7209804450539/60
                        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                        cursor = conn.cursor()
                        cursor.execute("INSERT INTO tempos_vazao (tempo,solenoide, dat_gravacao) VALUES (0,"+str(plantas[i].pino_atuador)+",current_timestamp)")
                        conn.commit()
                        conn.close()
                    vazao = vazao/2

                    #print("Vazao: ",vazao)

                    try:
                        lrn = plantas[i].lrn(pam=plantas[i].pam(area_molhada_emissor=cnt.area_molhada_emissor, area_total_planta=cnt.area_total_planta),capacidade_campo=cnt.capacidade_campo,umidade_atual=plantas[i].umidade_atual,profundidade_radicular=cnt.profundidade_radicular, densidade_solo=cnt.densidade_solo)
                        lrn_1 = definir_lrn(lrn=lrn,periodo=plantas[i].periodo_rega,vazao_gotejador=float(vazao/1000))
                        cond = 1
                    except:
                        print("Nao foi possivel calcular o tempo de rega\n")
                        plantas[i].tempo_rega = 0
                        cond = 0

                    if(plantas[i].modo_operacao==1 and cond == 1):
                        tempo_rega = ((lrn_1)/float(vazao/1000)) #+ ((lrn-cnt.volume_cano)/cnt.vazao_gotejador if((lrn-cnt.volume_cano)/cnt.vazao_gotejador)>0 else 0)
                        print(str(lrn_1)+" Litros "+"ID"+str(plantas[i].id)+"\n")
                        plantas[i].tempo_rega = (plantas[i].periodo_rega - 30) if tempo_rega > plantas[i].periodo_rega else tempo_rega
                    #if(plantas[i].modo_operacao == 3):
                        #plantas[i].tempo_rega = aprendizado(lm=lm,umidade_solo=plantas[i].umidade_atual,temperatura= temperatura,umidade_ar= umidade_ar, luminosidade=luminosidade,tempo_plantio= plantas[i].tempo_plantio+tempo_coleta_dados)/vazao
                    #Caso o sistema não esteja pausado
                    if(plantas[i].id==1):
                        tempo_de_rega_1 = plantas[i].tempo_rega
                    if(plantas[i].id==3):
                        tempo_de_rega_2 = plantas[i].tempo_rega 
                    if(plantas[i].modo_operacao != 2):#Quantidade de água em litros
                        if(plantas[i].id==2):
                            plantas[i].tempo_rega = tempo_de_rega_1
                        if(plantas[i].id==4):
                            plantas[i].tempo_rega = tempo_de_rega_2 
                        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                        cursor = conn.cursor()
                        cursor.execute("SELECT iluminacao from log_luminosidade order by time_log desc limit 1")
                        result = cursor.fetchall()
                        if (result==[]):
                            lum = 0
                        else:
                            lum = result[0][0]
                        conn.close()
                        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                        cursor = conn.cursor()
                        if(lum==1):
                            luminosidade = luminosidade_cima
                        else:
                            luminosidade = luminosidade_baixo
                        if(plantas[i].id==1):
                            plantas[i].umidade_atual = dados_umidade[0]
                        if(plantas[i].id==2):
                            plantas[i].umidade_atual = dados_umidade[1]
                        if(plantas[i].id==3):
                            plantas[i].umidade_atual = dados_umidade[2]
                        if(plantas[i].id==4):
                            plantas[i].umidade_atual = dados_umidade[3]
                        cursor.execute("INSERT INTO vlr_luminosidade (valor,dat_gravacao) VALUES("+str(luminosidade)+",current_timestamp)")
                        insert = "INSERT INTO dados (id_planta,umidade_atual,temperatura,umidade_ar,vazao,quantidade_agua,qtd_agua_calc,gotejador,luminosidade,tempo_plantado,modo_operacao, uv, dat_gravacao) VALUES ("
                        

                        if(plantas[i].tempo_regado == 0):
                            
                            if((plantas[i].id == 1 or plantas[i].id == 2) and not flag_tempo_rega_1):
                                print("id ",plantas[i].id ,"dif", plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio, " acumulado ", tempo_acumulado_1, " flag tempo ", flag_tempo_rega_1)
                                timetemp_1 = (plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio) - tempo_acumulado_1
                                query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao/1000)+","+str(timetemp_1*(vazao/1000))+","+ str(timetemp_1*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+"0"+"," + str(uv) + ",current_timestamp)"
                                if(plantas[i].id==2):
                                    tempo_acumulado_1 += timetemp_1
                            elif((plantas[i].id == 3 or plantas[i].id == 4) and not flag_tempo_rega_2):
                                print("id ",plantas[i].id ,"dif", plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio, " acumulado ", tempo_acumulado_2, " flag tempo ", flag_tempo_rega_2)
                                timetemp_1 = (plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio) - tempo_acumulado_2
                                if(plantas[i].id==4):
                                    tempo_acumulado_2 += timetemp_1
                                query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao/1000)+","+str(timetemp_1*(vazao/1000))+","+ str(timetemp_1*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+"0"+"," + str(uv) + ",current_timestamp)"

                            else:
                                query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao/1000)+","+"0"+"," + "0" +","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+str(plantas[i].modo_operacao)+"," + str(uv) + ",current_timestamp)"
                            cursor.execute(query) 
                        '''if(plantas[i].tempo_regado == 0):
                            if(plantas[i].tempo_rega > tempo_coleta_dados):
                                if(plantas[i].id == 1 or plantas[i].id == 2):
                                    print("id ",plantas[i].id ,"dif", plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio, " acumulado ", tempo_acumulado_1, " flag tempo ", flag_tempo_rega_1)
                                    timetemp_1 = (plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio) - tempo_acumulado_1 
                                    if(plantas[i].id==2):
                                        tempo_acumulado_1 += timetemp_1
                                if(plantas[i].id == 3 or plantas[i].id == 4):
                                    print("id ",plantas[i].id ,"dif", plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio, " acumulado ", tempo_acumulado_2, " flag tempo ", flag_tempo_rega_2)
                                    timetemp_1 = (plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio) - tempo_acumulado_2
                                    if(plantas[i].id==4):
                                        tempo_acumulado_2 += timetemp_1
                                query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao/1000)+","+str(timetemp_1*(vazao/1000))+","+ str(timetemp_1*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+"0"+"," + str(uv) + ",current_timestamp)"
                                if(plantas[i].id==2):
                                    plantas[0].tempo_rega_fim = 0
                                    if(flag_tempo_rega_1):
                                        tempo_acumulado_1 = 0
                                if(plantas[i].id==4):
                                    plantas[2].tempo_rega_fim = 0
                                    if(flag_tempo_rega_2):
                                        tempo_acumulado_2 = 0
        
                            else:
                                query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao/1000)+","+"0"+"," + "0" +","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+str(plantas[i].modo_operacao)+"," + str(uv) + ",current_timestamp)"
                            cursor.execute(query)  '''                          

                                             
                        if(plantas[i].tempo_regado > 0):
                            
                            #plantas[i].tempo_rega > tempo_coleta_dados and
                            if((plantas[i].id==1 or plantas[i].id==2) and not flag_tempo_rega_1):
                                print("id ", plantas[i].id," dif ", plantas[i].tempo_rega_fim - plantas[i].tempo_rega_inicio, " tempo regado ", plantas[i].tempo_regado, "flag", flag_tempo_rega_1)
                                tempo_acumulado_1 = (plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio) if ((plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio) > 0) else 0
                                plantas[i].tempo_regado = plantas[i].tempo_regado + ((plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio) if ((plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio) > 0) else 0)
                                query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao)+","+str(plantas[i].tempo_regado*(vazao/1000))+"," + str(plantas[i].tempo_rega*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+str(plantas[i].modo_operacao)+"," + str(uv) + ",current_timestamp)"
                                cursor.execute(query)
                                plantas[i].tempo_regado = 0
                            #plantas[i].tempo_rega > tempo_coleta_dados and
                            elif((plantas[i].id==3 or plantas[i].id==4) and not flag_tempo_rega_2): 
                                print("id ", plantas[i].id," dif ", plantas[i].tempo_rega_fim - plantas[i].tempo_rega_inicio, " tempo regado ", plantas[i].tempo_regado, "flag", flag_tempo_rega_2)  
                                tempo_acumulado_2 = (plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio) if ((plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio) > 0) else 0
                                plantas[i].tempo_regado = plantas[i].tempo_regado + ((plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio) if ((plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio) > 0) else 0)
                                query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao)+","+str(plantas[i].tempo_regado*(vazao/1000))+"," + str(plantas[i].tempo_rega*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+str(plantas[i].modo_operacao)+"," + str(uv) + ",current_timestamp)"
                                cursor.execute(query)
                                plantas[i].tempo_regado = 0
                            else:
                                if(plantas[i].id==1 or plantas[i].id==2):
                                    print("id ",plantas[i].id ," tempo regado ", plantas[i].tempo_regado)
                                    query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao)+","+str(plantas[i].tempo_regado*(vazao/1000))+"," + str(plantas[i].tempo_rega*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+str(plantas[i].modo_operacao)+"," + str(uv) + ",current_timestamp)"
                                    plantas[i].tempo_regado = 0
                                    if(plantas[i].id==2):
                                        tempo_rega_1 = 0
                                        tempo_acumulado_1 = 0
                                    cursor.execute(query)
                                    
                                
                                elif(plantas[i].id==3 or plantas[i].id==4):
                                    print("id ",plantas[i].id ," tempo regado ", plantas[i].tempo_regado)
                                    query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao)+","+str(plantas[i].tempo_regado*(vazao/1000))+"," + str(plantas[i].tempo_rega*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+str(plantas[i].modo_operacao)+"," + str(uv) + ",current_timestamp)"
                                    plantas[i].tempo_regado = 0
                                    if(plantas[i].id==4):
                                        tempo_rega_2 = 0
                                        tempo_acumulado_2 = 0
                                    cursor.execute(query)

                        if(plantas[i].modo_operacao == 1):
                            #query = insert + str(plantas[i].id)+"," + str(plantas[i].umidade_atual)+"," + str(temperatura)+"," + str(umidade_ar)+"," + str(vazao)+"," + str(plantas[i].tempo_rega*(vazao/1000))+","+str(plantas[i].calc_agua(mLitros)*plantas[i].tempo_rega/1000)+"," + str(luminosidade)+"," + str(plantas[i].tempo_plantio+tempo_coleta_dados)+ ","+str(plantas[i].modo_operacao)+"," + str(uv) + ",current_timestamp)"
                            #cursor.execute("UPDATE plantas set tempo_rega="+str(plantas[i].tempo_rega)+" WHERE id_planta="+str(plantas[i].id))
                            plantas[i].periodo_rega_fim = 0
                            plantas[i].periodo_rega_inicio = 0
                            #plantas[i].tempo_regado = 0
                            if(plantas[i].id==2):
                                tempo_rega_1 = 0
                            if(plantas[i].id==4):
                                tempo_rega_2 = 0
                            #cursor.execute(query)
                        
                        cursor.execute("UPDATE plantas set tempo_plantio="+str(plantas[i].tempo_plantio+tempo_coleta_dados+60)+" where id_planta="+str(plantas[i].id))
                        conn.commit()
                        conn.close()
                        
                        
            for i in range(len(plantas)):

                if(plantas[i].modo_operacao == 0 and plantas[i].qtd_agua > -1):
                    if(int(plantas[i].pino_atuador)==23):
                        vazao = (60.2862794402195-(0.000866903317118373*mLitros)+(0.0000000058674550482351*mLitros*mLitros)-(0.0000000000103544854360592*mLitros*mLitros*mLitros))/60
                    else:
                        vazao = (52.7209804450539-(0.000830712579866397*mLitros)-(0.00000000746003172622697*mLitros*mLitros)-(0.00000000000639293147077318*mLitros*mLitros*mLitros))/60
                    vazao = vazao/2
                    plantas[i].tempo_rega = plantas[i].qtd_agua/vazao

                plantas[i].periodo_rega_fim = time.time()
                antigo_periodo_rega_1 = plantas[0].periodo_rega
                antigo_periodo_rega_2 = plantas[2].periodo_rega
                
                #Se estiver no período de rega, inicia a irrigação
                if(((plantas[i].periodo_rega_fim - plantas[i].periodo_rega_inicio) >  plantas[i].periodo_rega) and plantas[i].periodo_rega > 0 and (plantas[i].id==1 or plantas[i].id==3) and plantas[i].ativo==0):
                    if(plantas[i].tempo_rega > 0):
                        print("Abrindo","Id "+str(plantas[i].id)+" Tempo de rega "+str(plantas[i].tempo_rega)+"\n")
                        plantas[i].acionar_irrigacao()
                        if(plantas[i].id == 1):
                            flag_tempo_rega_1 = False
                        if(plantas[i].id == 3):
                            flag_tempo_rega_2 = False
                        plantas[i].ativo = 1
                        plantas[i].tempo_rega_inicio = time.time()
                        plantas[i].periodo_rega_inicio = time.time()
                        x = datetime.now()
                        print(x.strftime("%H:%M:%S"))


                if(plantas[i].id == 1 or plantas[i].id == 2 ):
                    if(flag_tempo_rega_1==False):            
                        plantas[i].tempo_rega_fim = time.time()
                if(plantas[i].id == 3 or plantas[i].id == 4 ):
                    if(flag_tempo_rega_2==False):            
                        plantas[i].tempo_rega_fim = time.time()
                
                

                #Verifica se já passou o tempo de irrigação
                if((plantas[i].tempo_rega_fim - plantas[i].tempo_rega_inicio) > plantas[i].tempo_rega and (plantas[i].id==1 or plantas[i].id==3)):
                    if(plantas[i].ativo==1):
                        if(plantas[i].id==1):
                            plantas[0].tempo_regado+=plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio - tempo_acumulado_1
                            plantas[1].tempo_regado+=plantas[0].tempo_rega_fim - plantas[0].tempo_rega_inicio - tempo_acumulado_1
                            print("Tempo regado ao fechar ",plantas[0].tempo_regado)
                            flag_tempo_rega_1 = True
                            
                            #if(plantas[1].tempo_regado > tempo_coleta_dados):
                             #   plantas[0].tempo_regado = plantas[0].tempo_regado - tempo_acumulado_1#*tempo_coleta_dados)
                              #  plantas[1].tempo_regado = plantas[0].tempo_regado#*tempo_coleta_dados)
                            tempo_acumulado_1 = 0

                        if(plantas[i].id==3):
                            plantas[2].tempo_regado+=plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio - tempo_acumulado_2
                            plantas[3].tempo_regado+=plantas[2].tempo_rega_fim - plantas[2].tempo_rega_inicio - tempo_acumulado_2
                            flag_tempo_rega_2 = True
                            #if(plantas[3].tempo_regado > tempo_coleta_dados):
                             #   plantas[2].tempo_regado = plantas[2].tempo_regado - tempo_acumulado_2#*tempo_coleta_dados)
                              #  plantas[3].tempo_regado = plantas[2].tempo_regado #*tempo_coleta_dados)
                            tempo_acumulado_2 = 0
                        print("Fechou ","Id "+str(plantas[i].id)+" Tempo de rega "+str(plantas[i].tempo_rega_fim - plantas[i].tempo_rega_inicio)+"\n")
                        x = datetime.now()
                        print(x.strftime("%H:%M:%S"))
                        plantas[i].desativar_irrigacao()
                        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                        cursor = conn.cursor()
                        cursor.execute("SELECT tempo FROM tempos_vazao where solenoide="+str(plantas[i].pino_atuador)+"order by dat_gravacao desc limit 1")
                        result = cursor.fetchall()
                        tempo = int(result[0][0])
                        cursor.execute("select sum(quantidade_agua) from dados where dat_gravacao between (select dat_gravacao from tempos_vazao where tempo = 0 order by dat_gravacao desc limit 1) and current_timestamp")
                        result = cursor.fetchall()
                        try:
                            litros = float(result[0][0])
                        except:
                            if(result[0][0]==None):
                                litros = 0
                            else:
                                litros = float(result[0][0])
                        conn.close()
                        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
                        cursor = conn.cursor()
                        cursor.execute("INSERT INTO tempos_vazao(tempo,solenoide,dat_gravacao) VALUES("+str(tempo+plantas[i].tempo_rega)+","+str(plantas[i].pino_atuador)+", current_timestamp)")
                        conn.commit()
                        plantas[i].ativo = 0
                        conn.close()
                        
                        if(litros > 12):#Alterar aqui
                            try:
                                smail.enviar("Abasteça o tanque de água")
                            except:
                                print("Não foi possível enviar o email\n")
        except Exception as error:
            print(str(error))
            print("\n")
            conn.close()
            continue;

except KeyboardInterrupt:
    sea.sair()
    sys.exit(1)
except RuntimeError as error:
    print(str(error))
    print("\n")  

def aprendizado(lm,temperatura,umidade_solo,umidade_ar,luminosidade,tempo_plantio):
    colunas = ['Umidade Solo','Umidade Ar', 'Temperatura', 'Tempo Plantio','Luminosidade']
    data =  {
                'Umidade Solo': umidade_solo,
                'Umidade Ar' : umidade_ar,
                'Temperatura' : temperatura,
                'Tempo Plantio' : tempo_plantio,
                'Luminosidade' : luminosidade
            }
    df = pd.DataFrame (data, columns=colunas)
    Y = lm.preditc(df)
    return Y[0]
