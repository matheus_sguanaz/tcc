import RPi.GPIO as GPIO


class Planta:
    def __init__(self, id, pino_sensor, pino_atuador,periodo_rega, tempo_rega, umidade_desejada, umidade_atual,tempo_plantio,modo_operacao, qtd_agua, flag_novo_modo):
        self.id = id
        self.pino_sensor = pino_sensor
        self.pino_atuador = pino_atuador
        self.periodo_rega = periodo_rega
        self.tempo_rega = tempo_rega
        self.umidade_desejada = umidade_desejada
        self.umidade_atual = umidade_atual
        self.tempo_plantio = tempo_plantio
        self.modo_operacao = modo_operacao
        self.periodo_rega_inicio = 0
        self.periodo_rega_fim = 0
        self.tempo_rega_inicio = 0
        self.tempo_rega_fim = 0
        self.ativo = 0
        self.tempo_regado = 0
        self.qtd_agua = qtd_agua
        self.flag_novo_modo = flag_novo_modo 
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(int(self.pino_atuador), GPIO.OUT)
        GPIO.output(int(self.pino_atuador), GPIO.HIGH)

    def acionar_irrigacao(self):
        #GPIO.output(self.pino_atuador,GPIO.HIGH)
        GPIO.output(int(self.pino_atuador), GPIO.LOW)
        print("irrigação ativada")

    def desativar_irrigacao(self):
        #GPIO.output(self.pino_atuador,GPIO.LOW)
        GPIO.output(int(self.pino_atuador), GPIO.HIGH)
        print("irrigação desativada")
    
    def calcular_umidade(self, tensao):
        self.umidade_atual=(tensao/3)*100
        #return (tensao/3)*100
    
    def pam(self,area_molhada_emissor, area_total_planta): #porcentagem
        return (area_molhada_emissor/area_total_planta)*100

    def lrn(self,pam, capacidade_campo, umidade_atual, profundidade_radicular, densidade_solo):#milimetros cúbicos
        if(capacidade_campo-umidade_atual<0):
            capacidade_campo = umidade_atual
        return (((capacidade_campo-umidade_atual)/10)*densidade_solo*profundidade_radicular*(pam/100))/0.92

    def calc_agua(self, agua_saida):
        if(self.id==1):
            return (27.6714196713456 - (0.00174706586576923*agua_saida) + (0.0000000008198370388714*agua_saida*agua_saida) - (0.0000000000121682780389182*agua_saida*agua_saida*agua_saida))/60
        elif(self.id==2):
            return (28.7704069456962 - (0.00181653673794686*agua_saida) + (0.00000000685037870063738*agua_saida*agua_saida) - (0.0000000000131070239523566*agua_saida*agua_saida*agua_saida))/60
        elif(self.id==3):
            return (33.9106599527234 - (0.00602769870936224*agua_saida) + (0.00000174654304163376*agua_saida*agua_saida) - (0.000000000199140621736539*agua_saida*agua_saida*agua_saida))/60
        elif(self.id==4):
            return (33.9106992606366 - (0.00713452941419196*agua_saida) + (0.00000228444189201586*agua_saida*agua_saida) - (0.000000000273757066846487*agua_saida*agua_saida*agua_saida))/60
        else:
            return 0

        #Caso a capacidade de campo e a umidade atual sejam passadas em % de volume, retirar a desnidade do solo da equação
    
        