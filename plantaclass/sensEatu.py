import time
import sys
import Adafruit_DHT
import RPi.GPIO as GPIO
from ADS1x15 import ADS1115
import board
import adafruit_dht
import alertaemail as smail 

#ADS 1 e 2 leitura de sensores de humidade de solo
adc1 = ADS1115(address=72)
adc2 = ADS1115(address=73)
#ADS 3 leitura de celulas solares e sensor UV
adc3 = ADS1115(address=75)


def coletar_dados(plantas):
    inicio = time.time()
    fim = 0
    tamanho = len(plantas)
    umidades = []
    temperatura = []
    luminosidade = []
    luminosidade_cima = []
    umidade_ar = []
    inicio_tempo = time.time()
    fim_tempo = 0
    result_umidades = []
    umidades_separar = []
    uv = []
    dhtDevice = adafruit_dht.DHT22(board.D5, False)
    while((fim_tempo - inicio_tempo)<60):
        fim_tempo = time.time()
        fim = time.time()
        #Faz leitura dos sensores a cada segundo durante 30 segundos
        if((fim - inicio)>1):
            inicio = time.time()
            #humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, 22)
            try:
                temperature = dhtDevice.temperature
                humidity = dhtDevice.humidity
            except RuntimeError as erro:
                temperature = None
                humidity = None
                print(erro)
            except:
                temperature = None
                humidity = None
                print(sys.exc_info())
            temperatura.append(temperature)
            luminosidade.append(adc3._read(6,2/3,None,0))
            time.sleep(0.1)
            luminosidade_cima.append(adc3._read(5,2/3,None,0))
            time.sleep(0.1)
            uv.append(adc3._read(7,4,None,0))
            time.sleep(0.1)
            umidade_ar.append(humidity)
            for i in range(tamanho):
                umidades.append(umidade_mapper(int(plantas[i].pino_sensor)))
                time.sleep(0.1)
    dhtDevice.exit()
    if (0 in luminosidade):
       luminosidade= list(filter((0).__ne__, luminosidade))
    if (0 in luminosidade_cima):
           luminosidade_cima= list(filter((0).__ne__, luminosidade_cima))
    if(0 in umidade_ar):
        umidade_ar = list(filter((0).__ne__, umidade_ar))
    if(0 in temperatura):   
        temperatura = list(filter((0).__ne__, temperatura))
    umidade_ar = list(filter((None).__ne__, umidade_ar))
    temperatura = list(filter((None).__ne__, temperatura))
    try:
        umidade_ar.remove(max(umidade_ar))
    except:
        print("Não foi possivel remover o maior valor de umidade do ar")
    try:
        temperatura.remove(max(temperatura))
    except:
        print("Não foi possivel remover o maior valor de temperatura")
    try:
        luminosidade.remove(max(luminosidade))
    except:
        print("Não foi possivel remover o maior valor da celula solar de baixo")
    try:
        luminosidade_cima.remove(max(luminosidade_cima))
    except:
        print("Não foi possivel remover o maior valor da celula solar de cima")
    try:
        result_temperatura = sum(temperatura)/len(temperatura)
    except:
        print("Não foi possível obter o valor de temperatura")
        result_temperatura = -1
    try:
        result_luminosidade = ((sum(luminosidade)/len(luminosidade)) * 5.5)/32768
    except:
        print("Não foi possível obter o valor da celula solar de baixo")
        result_luminosidade = 1.38
    try:    
        result_luminosidade_cima = ((sum(luminosidade_cima)/len(luminosidade_cima)) * 5.5)/32768
    except:
        print("Não foi possível obter o valor da celula solar de cima")
        result_luminosidade_cima = 1.38
    try:
        result_umidade_ar = sum(umidade_ar)/len(umidade_ar)
    except:
        print("Não foi possível obter o valor de umidade do ar")
        result_umidade_ar = -1
    try:
        result_uv = ((sum(uv)/len(uv)) * 1.2)/32768
    except:
        print("Não foi possível obter o valor de UV")
        result_uv = -1 
    print(umidades)
    #Separar umidades e calcular a média dos valores
    for i in range(tamanho): #tamanhp = 2 umidades = 10 
        j = i
        umidades_separar = []
        while(j<len(umidades)):
            umidades_separar.append(umidades[j])
            j+=tamanho
        if(0 in umidades_separar):
            umidades_separar = list(filter((0).__ne__, umidades_separar))
        try:
            if(sum(umidades_separar)/len(umidades_separar)>24000):
                smail.enviar("O sensor da planta de ID: "+i+" está com algum problema")
        except:
            print("Nao foi possivel calcular a media de umidade do solo")
        try:
            umidades_separar.remove(max(umidades_separar))
            umidades_separar.remove(max(umidades_separar))
        except:
            print("Não foi possível remover os valores mais altos")
        try:
            result_umidades.append(mapear(n_in=sum(umidades_separar)/len(umidades_separar),min_inp=23000,max_inp=7200,min_out=0,max_out=100))#Alterar Aqui
        except:
            print("Nao foi possivel calcular o valor de umidade do solo")
            result_umidades = [100,100,100,100]

    return result_umidades, result_temperatura, result_luminosidade,result_luminosidade_cima, result_umidade_ar, result_uv


def umidade_mapper(pino_sensor):#Alterar aqui valor do ganho, quais os pinoes e qual o adc
    if(pino_sensor==1):
        return adc1._read(4,1,None,0)
    elif(pino_sensor==2):
        return adc1._read(5,1,None,0)
    elif(pino_sensor==3):
        return adc1._read(6,1,None,0)
    elif(pino_sensor==4):
        return adc1._read(7,1,None,0)
    elif(pino_sensor==5):
        return adc2._read(4,1,None,0)
    elif(pino_sensor==6):
        return adc2._read(5,1,None,0)
    elif(pino_sensor==7):
        return adc2._read(6,1,None,0)

def mapear(n_in, min_inp, max_inp, min_out, max_out):
    return (((n_in - min_inp)/(max_inp-min_inp))*(max_out-min_out))+min_out

def sair():
    dhtDevice = adafruit_dht.DHT22(board.D5, False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(23, GPIO.OUT)
    GPIO.setup(26, GPIO.OUT)
    dhtDevice.exit()
    GPIO.cleanup()
    sys.exit(1)

        

