import psycopg2
import time
import RPi.GPIO as GPIO


conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
cursor = conn.cursor()

ini = time.time()
luminosidade = True
led_ativos = False
iluminacao = 0

GPIO.setmode(GPIO.BCM)
GPIO.setup(6, GPIO.OUT)
GPIO.output(6, GPIO.HIGH)
while(1):
    fim = time.time()  
   
    if(fim - ini > 300):
        cursor.execute("select valor from vlr_luminosidade order by dat_gravacao desc limit 1")
        select_luminosidade = cursor.fetchall()
        if(float(select_luminosidade[0][0])>1.37):#Alterar aqui
            luminosidade = True
        else:
            luminosidade = False
        ini = time.time()
        cursor.execute("SELECT now() - hora_zerar FROM controle_luminosidade")
        zerar_tempo = cursor.fetchall()
        try:
            cursor.execute("SELECT currval('iluminacao')")
        except:
            conn.rollback()
            cursor.execute("SELECT nextval('iluminacao')")
        conn.commit()
        result_2 = cursor.fetchall()
        iluminacao = int(result_2[0][0])
        print(iluminacao)
        if("1 day" in str(zerar_tempo[0][0])):
            cursor.execute("UPDATE controle_luminosidade set hora_forcada = now()")
            cursor.execute("UPDATE controle_luminosidade set hora_zerar = now()")
            conn.commit()
            cursor.execute("INSERT INTO dados_iluminacao (leds_ligados, tempo_iluminado, dia) VALUES (nextval('led_ativo'),nextval('iluminacao'),current_date)")
            cursor.execute("SELECT setval('iluminacao',1)")
            cursor.execute("SELECT setval('led_ativo',1)")
            conn.commit()
        cursor.execute("SELECT now() - hora_forcada FROM controle_luminosidade")
        forcar_ativ = cursor.fetchall()
        dif_for_ativ = str(forcar_ativ[0][0]).split(":")[0]
        if(not luminosidade and int(dif_for_ativ)>7 and int(dif_for_ativ)<19):
            cursor.execute("INSERT INTO log_luminosidade (iluminacao, time_log) VALUES (1, current_timestamp)")
            conn.commit()
            print("ativando leds")
            led_ativos = True
            GPIO.output(6, GPIO.LOW)
        else:
            led_ativos = False
            print("desativando led")
            GPIO.output(6, GPIO.HIGH)
            cursor.execute("INSERT INTO log_luminosidade (iluminacao, time_log) VALUES (0, current_timestamp)")
            conn.commit()
        if(luminosidade or led_ativos):
            cursor.execute("SELECT nextval('iluminacao')")#incrementar em valores maiores
            cursor.execute("SELECT nextval('iluminacao')")
            cursor.execute("SELECT nextval('iluminacao')")
            cursor.execute("SELECT nextval('iluminacao')")
            cursor.execute("SELECT nextval('iluminacao')")
            conn.commit()
        if(led_ativos):
            cursor.execute("SELECT nextval('led_ativo')")
            cursor.execute("SELECT nextval('led_ativo')")
            cursor.execute("SELECT nextval('led_ativo')")
            cursor.execute("SELECT nextval('led_ativo')")
            cursor.execute("SELECT nextval('led_ativo')")
            conn.commit()


#pg_dump.exe --host localhost --port 5432 --username postgres --inserts --column-inserts --file db.sql tcc
            