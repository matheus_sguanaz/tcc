import psycopg2
import RPi.GPIO as GPIO
import time
import plantaclass.alertaemail as smail 


#user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc" = user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc"
sensor_vazao_esquerda = 17
sensor_vazao_direita = 27
pino_atuador_esquerdo = 26
pino_atuador_direito = 23
tentativas_1 = 0
tentativas_2 = 0
tentativas_3 = 0
tentativas_4 = 0

#Considerando 


GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor_vazao_esquerda, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(sensor_vazao_direita, GPIO.IN, pull_up_down = GPIO.PUD_UP)


global count
global count_2
global start_counter
global start_counter_2
count = 0
count_2 = 0
start_counter = 0
start_counter_2 = 0
l_vazao = []
l_vazao_2 = []

def countPulse(channel):
   global count
   if start_counter == 1:
      count = count+1

def countPulse_2(channel):
    global count_2
    if start_counter_2 == 1:
      count_2 = count_2+1

GPIO.add_event_detect(sensor_vazao_esquerda,GPIO.FALLING,callback=countPulse)
GPIO.add_event_detect(sensor_vazao_direita,GPIO.FALLING,callback=countPulse_2)

while(1):
    conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
    cursor = conn.cursor()
    cursor.execute("SELECT ativo FROM controle_vazao where id_atuador ="+pino_atuador_esquerdo+" order by id_vazao desc limit 1")
    result = cursor.fetchall()
    tem_vazao = int(result[0][0])# Solenoide do pino 23
    cursor.execute("SELECT ativo FROM controle_vazao where id_atuador ="+pino_atuador_direito+" order by id_vazao desc limit 1")
    result = cursor.fetchall()
    tem_vazao_2 = int(result[0][0])#Solenoide do pino 26
    conn.close()

    start_counter = 1
    start_counter_2 = 1
    time.sleep(1)
    start_counter = 0
    start_counter_2 = 0
    vazao = (count * 60 * 2.25 / 1000)
    vazao_2 = (count_2 * 60 * 2.25 / 1000)
    count = 0 #Sensor esquerdo
    count_2 = 0#sernsor direito
    
    l_vazao.append(vazao)#sensor esquerdo
    l_vazao_2.append(vazao_2)#sensor direito


    if len(l_vazao)<3:
        l_vazao.append(0)
    if len(l_vazao_2)<3:
        l_vazao_2.append(0)

    #Se a vazão atualmente for zero mas anteriormente não
    if (vazao==0 and l_vazao[-2]!=0):
        print("inserindo no primeiro")
        l_vazao = list(filter((0).__ne__, l_vazao))
        vazao = sum(l_vazao)/len(l_vazao)
        #maximo = max(l_vazao)
        #l_vazao.remove(maximo)
        #vazao = max(l_vazao)
        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO vazoes(id_sensor,valor, dat_gravacao) VALUES ("+pino_atuador_esquerdo+","+str(vazao/60)+",current_timestamp)")#id_sensor se refere ao pino da solenoide ao qual o sensor de vazão está ligado
        conn.commit()
        conn.close()
        l_vazao = []
    #Se a vazão atualmente for zero mas anteriormente não
    if (vazao_2==0 and l_vazao_2[-2]!=0):
        print("inserindo no segundo")
        l_vazao_2=list(filter((0).__ne__, l_vazao_2))
        vazao_2 = sum(l_vazao_2)/len(l_vazao_2)
        #maximo = max(l_vazao)
        #l_vazao_2.remove(maximo)
        #vazao_2 = max(l_vazao_2)
        conn = psycopg2.connect(user="postgres",password="123",host="192.168.0.25", port="5432",database="tcc")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO vazoes(id_sensor,valor, dat_gravacao) VALUES ("+pino_atuador_direito+","+str(vazao_2/60)+",current_timestamp)")
        conn.commit()
        conn.close()
        l_vazao_2 = []
    if(vazao > 0 and tem_vazao == 0):
        #Está saindo água mas a solenoide não foi ativada
        tentativas_1 += 1
        if(tentativas_1 > 30):
            smail.enviar("Está saindo água mas a solenoide do pino 23 não foi ativada")
            tentativas_1 = 0
    if(vazao_2 > 0 and tem_vazao_2 == 0):
        #Está saindo água mas a solenoide não foi ativada
        tentativas_2 += 1
        if(tentativas_2 > 30):
            smail.enviar("Está saindo água mas a solenoide do pino 26 não foi ativada")
            tentativas_2 = 0
    if(vazao==0 and tem_vazao == 1):
        #Não está saindo água mas a solenoide foi ativada
        tentativas_3 += 1
        if(tentativas_3 > 30):
            smail.enviar("Não está saindo água mas a solenoide do pino 23 foi ativada")
            tentativas_3 = 0
    if(vazao_2==0 and tem_vazao_2 == 1):
        #Não está saindo água mas a solenoide foi ativada
        tentativas_4 += 4
        if(tentativas_4 > 30):
            smail.enviar("Não está saindo água mas a solenoide do pino 26 foi ativada")
            tentativas_4 = 0
    


            


    
        






    